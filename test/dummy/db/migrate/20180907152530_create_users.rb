class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.column :settings, "bit(32)"

      t.timestamps
    end
  end
end
